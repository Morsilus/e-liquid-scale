#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 09:19:22 2020

@author: robert
"""

from scale import Scale
import sys
import os
from os import path
from multiprocessing.pool import ThreadPool

pool = ThreadPool(processes=1)

USB0_path = "/dev/ttyUSB0"
USB1_path = "/dev/ttyUSB1"

measured = ["VG", "PG", "Nicotine"]

PRESET = [[85, 15, 0.5],
          [70, 30, 1.5],
          [75, 25, 1],
          [30, 70, 2.5],
          [50, 50, 2]]

PRESET_NIC = [[50, 20]]

FLAV_G = 1.04865
NIC_G = 1.00925
PG_G = 1.0373
VG_G = 1.2613

def_amount = 30
def_percs = [10, 0, 25, 75]


#---------

def run(percs = [], nicotine_pg = 0):
    os.system('cls' if os.name == 'nt' else 'clear')
    total = float(input("Total amount (ml):"))
    flavor = float(input("Flavor %: "))
    
    dist_ch_str = str("Get preset distribution\n0 - none")
    for i in range (0, len(PRESET)):
        dist_ch_str += ("\n{} - PG = {}, VG = {}, nic = {}".format(i + 1, 
                        PRESET[i][0], PRESET[i][1], PRESET[i][2]))
    dist_ch_str += "\n: "
    dist_choice = int(input(dist_ch_str))
    
    if (0 < dist_choice and dist_choice <= len(PRESET)):
        vg = PRESET[dist_choice - 1][0]
        pg = PRESET[dist_choice - 1][1]
        nic = PRESET[dist_choice - 1][2]
    else:
        vg = float(input("VG %: "))
        pg = float(input("PG %: "))
        nic = float(input("Nicotine %: "))
        
    if (nic != 0):
        dist_ch_str = str("Get preset nicotine\n0 - none")
        for i in range (0, len(PRESET_NIC)):
            dist_ch_str += ("\n{} - PG = {}, VG = {}, nic = {}%".format(i + 1, 
                            PRESET_NIC[i][0], 100 - PRESET_NIC[i][0], PRESET_NIC[i][1]))
        dist_ch_str += "\n: "
        dist_choice = int(input(dist_ch_str))
        if (0 < dist_choice and dist_choice <= len(PRESET)):
            nic_pg = PRESET_NIC[dist_choice - 1][0]
            nic_s = PRESET_NIC[dist_choice - 1][1]
        else:
            nic_s = float(input("Nicotine strength %: "))
            nic_pg = float(input("Nicotine PG %: "))
        nic_g = calcNicG(nic_s, nic_pg)
    
    flavor_a = total * flavor / 100
    nic_a = nic / nic_s * total
    pg_a = pg / 100 * total - (flavor_a + nic_a * nic_pg / 100)
    vg_a = vg / 100 * total - nic_a * (100 - nic_pg) / 100
    if (((pg + vg) == 100) and (pg_a >= 0) and (vg_a >= 0)):
        offset = - printWeight("Place empty bottle and press enter ({:.2f}g)")
        print("\n\n")
        offset = - loadItem("Flavor", flavor, flavor_a * FLAV_G, offset);
        offset = - loadItem("Nicotine", nic, nic_a * nic_g, offset);
        offset = - loadItem("PG", pg, pg_a * PG_G, offset);
        offset = - loadItem("VG", vg, vg_a * VG_G, offset);
    else:
        print("Wrong percentage distribution")
        
def loadItem(name, perc, weight, offset):
    if (perc != 0):
        return printWeight("{} {:.2f}%, {:.2f}g: ".format(name, perc, weight) + "{:.2f}g", offset)
    return -offset

def printWeight(prompt, offset = 0):
    async_result = pool.apply_async(readCmd)
    weight = scale.readScale()
    while not async_result.ready():
        print(prompt.format(weight + offset), end = "\r")
        weight = scale.readScale()
        sys.stdout.write('\033[2K\033[1G')
    sys.stdout.write('\033[2K\033[1G')
    return weight

def readCmd():
    return input()

def calcNicG(nic_s, nic_pg):
    return (nic_s / 100) * NIC_G + (100 - nic_s) / 100 * ((100 - nic_pg) / 100 * VG_G 
            + nic_pg / 100 * PG_G)

#----------

if ((len(sys.argv) >= 2) and (path.lexists(sys.argv[1]))):
    scale = Scale(sys.argv[1])      
elif (path.lexists(USB0_path)):
    scale = Scale(USB0_path)
elif( path.lexists(USB1_path)):
    scale = Scale(USB1_path)
else:
    print("Error could not open any port")
    
if (len(sys.argv) > 2):
    measured = sys.argv[2 : len(sys.argv)]
    

run()            
