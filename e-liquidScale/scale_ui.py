#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 11 17:38:34 2020

@author: robert
"""


from scale import Scale
import sys
import os
from os import path
from multiprocessing.pool import ThreadPool

pool = ThreadPool(processes=1)

USB0_path = "/dev/ttyUSB0"
USB1_path = "/dev/ttyUSB1"

def run():
    os.system('cls' if os.name == 'nt' else 'clear')
    offset = 0
    while True:
        offset = - printWeight("Weight : {:.2f}g", offset)

def printWeight(prompt, offset = 0):
    async_result = pool.apply_async(readCmd)
    weight = scale.readScale()
    while not async_result.ready():
        print(prompt.format(weight + offset), end = "\r")
        weight = scale.readScale()
        sys.stdout.write('\033[2K\033[1G')
    sys.stdout.write('\033[2K\033[1G')
    return weight

def readCmd():
    return input()

#----------

if ((len(sys.argv) >= 2) and (path.lexists(sys.argv[1]))):
    scale = Scale(sys.argv[1])      
elif (path.lexists(USB0_path)):
    scale = Scale(USB0_path)
elif( path.lexists(USB1_path)):
    scale = Scale(USB1_path)
else:
    print("Error could not open any port")
      

run()            
