#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 21:59:44 2020

@author: robert
"""

import serial
import struct
import random
from os import path

OUT_HANDSHAKE = 0x10
OUT_CALIB = 0x11

IN_HANDSHAKE = 0x01
IN_SCALE = 0x02
        
class Scale:
    def __init__(self, ser_path):
        if (path.lexists(ser_path)):
            self.ser = serial.Serial(ser_path, 9600, timeout = 1)
        
    def __del__(self):
        self.ser.close()
        
    def handshake(self):
        handshake = bytearray(struct.pack('f', random.random()))
        handshake.insert(0, OUT_HANDSHAKE)
        self.ser.flush()
        self.ser.write(handshake)
        hs_resp = self.ser.read(5)
        print(hs_resp[0])
        if ((len(hs_resp) == 5) and (hs_resp[0] == IN_HANDSHAKE)):
            return True
        else:
            return False
        
    def readScale(self):
        in_bytes = self.ser.read(5)
        if (len(in_bytes) == 5):
            return (struct.unpack('f', in_bytes[1:5]))[0]
            print(in_bytes, end='\r')
        else:
            return 0.0
        
    def calibrate(self, weight):
        calibrate = bytearray(struct.pack('f', weight))
        calibrate.insert(0, OUT_CALIB)