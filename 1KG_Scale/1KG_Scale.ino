#include <HX711.h>

#define OUT_HANDSHAKE 0x01
#define OUT_SEND 0x02

#define IN_HANDSHAKE 0x10
#define IN_CALIB 0x11
#define IN_AUTO 0x12
#define IN_SEND 0x13

#define DOUT 7
#define SCK 8

HX711 scale(DOUT, SCK);

float calibration_factor = 1066;
float units;

union {
  float units;
  byte bUnits[4];
} uScale;

bool autoSend = true;

void setup() {
  Serial.begin(9600);

  scale.set_scale();
  scale.tare();  //Reset the scale to 0

  long zero_factor = scale.read_average(); //Get a baseline reading
  //This can be used to remove the need to tare the scale. Useful in permanent scale projects.

  scale.set_scale(calibration_factor);
}

void loop() {
  refreshUnits();

  if(Serial.available()) {
    char* buffer = (byte*) malloc(5 * sizeof(byte));
    byte n = Serial.readBytes(buffer, 5);
    if (true) {
      menu(buffer);
    }
  }
  
  if (autoSend) {
    sendScale();
  }
}

void sendScale() {
  Serial.write(OUT_SEND);
  Serial.write(uScale.bUnits, 4);
}

void refreshUnits() {
  uScale.units =  scale.get_units(10);
  if (uScale.units < 0)
  {
    uScale.units = 0.00;
  }
}

void menu(byte* data) {
  switch(data[0]) {
    case IN_CALIB:
      union {
        float weight;
        byte bWeight[4];
      } uTemp;
      memcpy(data + 1, uTemp.bWeight, 4);
      calibrate(uTemp.weight);
      break;
    case IN_AUTO:
      autoSend = data[4];
      break;
    case IN_SEND:
      sendScale();
      break;
    case IN_HANDSHAKE:
      data[0] = OUT_HANDSHAKE;
      Serial.write(data, 5);
      break;
  }
}

void calibrate(float weight) {
  refreshUnits();
  calibration_factor *= (units / weight);
  scale.set_scale(calibration_factor);
  refreshUnits();
}
